import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoParticipantePage } from './info-participante.page';

describe('InfoParticipantePage', () => {
  let component: InfoParticipantePage;
  let fixture: ComponentFixture<InfoParticipantePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoParticipantePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoParticipantePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
