import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-info-participante',
  templateUrl: './info-participante.page.html',
  styleUrls: ['./info-participante.page.scss'],
})
export class InfoParticipantePage implements OnInit {

  alumno: any;
  facturas={};
  cursos={};
  vencidas={};

  constructor(private route:ActivatedRoute, private nav:NavController) {
    setTimeout(() => {
      this.nav.navigateRoot(["home"]);
    }, 20000);
   }
   
  ngOnInit() {
    this.alumno = this.route.snapshot.paramMap;
    console.log("DATO en participante")
    console.log(this.alumno);
    console.log(this.alumno.params);
    this.alumno = this.alumno.params;
    console.log("foto "+ this.alumno.foto);
    
    this.cursos=JSON.parse(this.alumno.cursos);
    this.facturas=JSON.parse(this.alumno.facturas);
    this.vencidas=JSON.parse(this.alumno.vencidas);

  }
  irHome(){
    this.nav.navigateRoot(["home"]);
  }


}
