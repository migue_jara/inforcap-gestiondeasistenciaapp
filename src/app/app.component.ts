import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { OSNotificationPayload, OneSignal } from '@ionic-native/onesignal/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private oneSignal: OneSignal,
    private nav: NavController,
    private screenOrientation: ScreenOrientation,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      /*Para pantalla Horizontal*/
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);

      /*Para One Signal*/
      this.oneSignal.startInit('cda9ee56-5b84-45ca-9cef-9d72f8e22716', '3567533338');
      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.None);
      this.oneSignal.handleNotificationReceived().subscribe(data => this.onPushReceived(data.payload));
      this.oneSignal.endInit();

    });
  }
  private onPushReceived(payload: OSNotificationPayload) {

    console.log("receive push" + payload.additionalData)
    this.nav.navigateRoot(["/info-participante", payload.additionalData])

  }

  private onPushOpened(payload: OSNotificationPayload) {
    this.nav.navigateRoot(["info-participante", payload.additionalData])
  }
}
