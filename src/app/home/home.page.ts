import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(private plat: Platform) {
    this.backButton();
  }

  ngOnInit() {}
  
  backButton() {
    this.plat.backButton.subscribeWithPriority(9999, () => { }).unsubscribe();
  }

}
